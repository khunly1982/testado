﻿CREATE TABLE [dbo].[MovieActor]
(
	ActorId INT FOREIGN KEY REFERENCES Actor ON DELETE CASCADE,
	MovieId INT FOREIGN KEY REFERENCES Movie ON DELETE CASCADE,
)
