﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

DECLARE @i INT = 0;
DECLARE @j INT = 0;

WHILE(@i < 250)
BEGIN
    SET @i = @i+1;
    INSERT INTO Movie VALUES(@i, 'Movie ' + CONVERT(VARCHAR, @i))
END

WHILE(@j < 250)
BEGIN
    SET @j = @j+1;
    INSERT INTO Actor VALUES(@j, 'Actor ' + CONVERT(VARCHAR, @j))
END

SET @i=0;
SET @j=0;

WHILE(@i < 250)
BEGIN
    SET @i = @i+1;
    WHILE(@j < 250)
    BEGIN
        SET @j = @j+1;
        INSERT INTO MovieActor VALUES(@j, @i);
    END
    SET @j=0;
END