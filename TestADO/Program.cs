﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace TestADO
{
    class V_MovieActor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ActorId { get; set; }
        public string ActorName { get; set; }
    }

    class MovieActor
    {
        public int ActorId { get; set; }
        public int MovieId { get; set; }
    }

    class Movie
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Actor> Actors { get; set; }
    }
    class Actor
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }


    class Program
    {
        const string CONNECTION_STRING = "server=K-PC;database=TestADO;integrated security=true";

        static void Main(string[] args)
        {
            Console.WriteLine("Test requète (SqlServer Local) ManyToMany 250 X 250");

            TestAsync("1. Pas de jointure", () =>
            {
                IEnumerable<Movie> movies =  GetMovies().Select(m => {
                    m.Actors = GetActorsByMovie(m.Id).ToList();
                    return m;
                }).ToList();
            });

            TestAsync("2. Jointure avec GroupBy Linq", () =>
            {
                IEnumerable<Movie> movies = GetMoviesWithActors().ToList();
            });

            TestAsync("3. Jointure sans GroupBy Linq", () =>
            {
                IEnumerable<Movie> movies = GetMoviesWithActors2().ToList();
            });

            TestAsync("4. Jointure avec Linq", () =>
            {
                IEnumerable<Movie> movies = GetMovieActors()
                .Join(GetActors(), j => j.ActorId, a => a.Id,(j, a) => new { Actor = a, MovieId = j.MovieId })
                    .Join(GetMovies(), j => j.MovieId,  m => m.Id, (j,m) => new { Actor = j.Actor, Movie = m })
                    .GroupBy(m => m.Movie.Id).Select(g => new Movie { Id = g.Key, Name = g.FirstOrDefault().Actor.Name, Actors = g.Select(a => new Actor
                    {
                        Id = a.Actor.Id,
                        Name = a.Actor.Name
                    }).ToList()}).ToList();
            });

            Console.ReadKey();
        }

        static async void TestAsync(string testName, Action action)
        {
            Console.WriteLine($"Begin {testName}");
            DateTime start = DateTime.Now;
            await Task.Run(action);
            double duration = (DateTime.Now - start).TotalMilliseconds;
            Console.WriteLine($"End {testName} - duration: {duration} ms");
        }

        static IEnumerable<Actor> GetActors()
        {
            using (SqlConnection c = new SqlConnection(CONNECTION_STRING))
            {
                c.Open();
                using (SqlCommand cmd = c.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM Actor";
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            yield return new Actor
                            {
                                Id = (int)reader["Id"],
                                Name = (string)reader["Name"],
                            };
                        }
                    }

                }
            }
        }

        static IEnumerable<MovieActor> GetMovieActors()
        {
            using (SqlConnection c = new SqlConnection(CONNECTION_STRING))
            {
                c.Open();
                using (SqlCommand cmd = c.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM MovieActor";
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            yield return new MovieActor
                            {
                                ActorId = (int)reader["ActorId"],
                                MovieId = (int)reader["MovieId"]
                            };
                        }
                    }

                }
            }
        }

        static IEnumerable<Actor> GetActorsByMovie(int movieId)
        {
            using (SqlConnection c = new SqlConnection(CONNECTION_STRING))
            {
                c.Open();
                using (SqlCommand cmd = c.CreateCommand())
                {
                    cmd.CommandText = "SELECT a.* FROM Actor a JOIN MovieActor j ON a.Id = j.actorId WHERE MovieId = @param";
                    cmd.Parameters.AddWithValue("@param", movieId);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            yield return new Actor { 
                                Id = (int)reader["Id"],
                                Name = (string)reader["Name"],
                            };
                        }
                    }
                    
                } 
            }
        }

        static IEnumerable<Movie> GetMovies()
        {
            using (SqlConnection c = new SqlConnection(CONNECTION_STRING))
            {
                c.Open();
                using (SqlCommand cmd = c.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM Movie";
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            yield return new Movie
                            {
                                Id = (int)reader["Id"],
                                Name = (string)reader["Name"],
                            };
                        }
                    }

                }
            }
        }

        static IEnumerable<Movie> GetMoviesWithActors()
        {
            return GetVMovieActors().GroupBy(v => v.Id)
                .Select(g => new Movie
                {
                    Id = g.Key,
                    Name = g.FirstOrDefault().Name,
                    Actors = g.Select(v => new Actor
                    {
                        Id = v.ActorId,
                        Name = v.ActorName,
                    }).ToList()
                });
        }

        static IEnumerable<V_MovieActor> GetVMovieActors()
        {
            using (SqlConnection c = new SqlConnection(CONNECTION_STRING))
            {
                c.Open();
                using (SqlCommand cmd = c.CreateCommand())
                {
                    cmd.CommandText = "SELECT m.Id, m.[Name], a.Id AS actorId, a.[Name] AS ActorName FROM Movie m JOIN MovieActor j ON m.Id = j.movieId JOIN Actor a ON a.Id = j.ActorId";
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            yield return new V_MovieActor
                            {
                                Id = (int)reader["Id"],
                                Name = (string)reader["Name"],
                                ActorId = (int)reader["Id"],
                                ActorName = (string)reader["ActorName"],
                            };
                        }
                    }

                }
            }
        }

        static IEnumerable<Movie> GetMoviesWithActors2()
        {
            using (SqlConnection c = new SqlConnection(CONNECTION_STRING))
            {
                c.Open();
                using (SqlCommand cmd = c.CreateCommand())
                {
                    cmd.CommandText = "SELECT m.Id, m.[Name], a.Id AS actorId, a.[Name] AS ActorName FROM Movie m JOIN MovieActor j ON m.Id = j.movieId JOIN Actor a ON a.Id = j.ActorId";
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        List<Movie> result = new List<Movie>();
                        while (reader.Read())
                        {
                            int mId = (int)reader["Id"];
                            Actor actor = new Actor
                            {
                                Id = (int)reader["ActorId"],
                                Name = (string)reader["ActorName"],
                            };
                            Movie m = result.FirstOrDefault(x => x.Id == mId);
                            if (m is null)
                            {
                                result.Add(new Movie
                                {
                                    Id = mId,
                                    Name = (string)reader["Name"],
                                    Actors = new List<Actor>{ actor }
                                });
                            }
                            else
                            {
                                m.Actors.Add(actor);
                            }
                        }
                        return result;
                    }

                }
            }
        }
    }
}
